package com.btechmajorproject2020uemk.browl.models.main

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DeviceCard(

    @Expose
    @SerializedName("name")
    val cardName: String? = null,

    @Expose
    @SerializedName("deviceId")
    val deviceId: String? = null,

    @Expose
    @SerializedName("error_message")
    val errorMessage: String? = null
) {
    override fun toString(): String {
        return "DeviceCard(cardName=$cardName, deviceId=$deviceId, errorMessage=$errorMessage)"
    }
}
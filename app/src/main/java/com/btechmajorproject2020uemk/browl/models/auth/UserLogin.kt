package com.btechmajorproject2020uemk.browl.models.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserLogin(

    @Expose
    @SerializedName("name")
    val userName: String? = null,

    @Expose
    @SerializedName("email")
    val userEmail: String? = null,

    @Expose
    @SerializedName("token")
    val authToken: String? = null,

    @Expose
    @SerializedName("error_message")
    val errorMessage: String? = null
) {
    override fun toString(): String {
        return "UserLogin(userName=$userName, userEmail=$userEmail, authToken=$authToken, errorMessage=$errorMessage)"
    }
}
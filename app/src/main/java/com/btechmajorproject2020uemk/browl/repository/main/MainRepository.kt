package com.btechmajorproject2020uemk.browl.repository.main

import androidx.lifecycle.LiveData
import com.btechmajorproject2020uemk.browl.api.RetrofitBuilder
import com.btechmajorproject2020uemk.browl.models.main.DeviceCard
import com.btechmajorproject2020uemk.browl.repository.NetworkBoundResource
import com.btechmajorproject2020uemk.browl.ui.main.addCard.state.AddCardViewState
import com.btechmajorproject2020uemk.browl.ui.main.home.state.HomeViewState
import com.btechmajorproject2020uemk.browl.util.ApiSuccessResponse
import com.btechmajorproject2020uemk.browl.util.DataState
import com.btechmajorproject2020uemk.browl.util.GenericApiResponse

object MainRepository {

    fun addCard(
        authToken: String,
        cardName: String,
        deviceId: String
    ): LiveData<DataState<AddCardViewState>> {
        return object : NetworkBoundResource<DeviceCard, AddCardViewState>() {
            override fun handleApiSuccessResponse(response: ApiSuccessResponse<DeviceCard>) {
                result.value = DataState.data(
                    data = AddCardViewState(
                        deviceCard = response.body
                    )
                )
            }

            override fun createCall(): LiveData<GenericApiResponse<DeviceCard>> {
                return RetrofitBuilder.userApiService.addNewCard(authToken, cardName, deviceId)
            }

        }.asLiveData()
    }

    fun getAllCards(authToken: String): LiveData<DataState<HomeViewState>> {
        return object : NetworkBoundResource<List<DeviceCard>, HomeViewState>() {
            override fun handleApiSuccessResponse(response: ApiSuccessResponse<List<DeviceCard>>) {
                result.value = DataState.data(
                    data = HomeViewState(
                        deviceCard = response.body
                    )
                )
            }

            override fun createCall(): LiveData<GenericApiResponse<List<DeviceCard>>> {
                return RetrofitBuilder.userApiService.getAllCards(authToken)
            }

        }.asLiveData()
    }

    fun deleteCard(authToken: String, deviceId: String): LiveData<DataState<HomeViewState>> {
        return object : NetworkBoundResource<List<DeviceCard>, HomeViewState>() {
            override fun handleApiSuccessResponse(response: ApiSuccessResponse<List<DeviceCard>>) {
                result.value = DataState.data(
                    data = HomeViewState(
                        deviceCard = response.body
                    )
                )
            }

            override fun createCall(): LiveData<GenericApiResponse<List<DeviceCard>>> {
                return RetrofitBuilder.userApiService.deleteCard(authToken, deviceId)
            }

        }.asLiveData()
    }
}
package com.btechmajorproject2020uemk.browl.repository.auth

import androidx.lifecycle.LiveData
import com.btechmajorproject2020uemk.browl.api.RetrofitBuilder
import com.btechmajorproject2020uemk.browl.models.auth.UserLogin
import com.btechmajorproject2020uemk.browl.models.auth.UserRegistration
import com.btechmajorproject2020uemk.browl.repository.NetworkBoundResource
import com.btechmajorproject2020uemk.browl.ui.auth.state.AuthViewState
import com.btechmajorproject2020uemk.browl.util.ApiSuccessResponse
import com.btechmajorproject2020uemk.browl.util.DataState
import com.btechmajorproject2020uemk.browl.util.GenericApiResponse

object AuthRepository {

    fun userLogin(userEmail: String, userPassword: String): LiveData<DataState<AuthViewState>> {
        return object : NetworkBoundResource<UserLogin, AuthViewState>() {
            override fun handleApiSuccessResponse(response: ApiSuccessResponse<UserLogin>) {
                result.value = DataState.data(
                    data = AuthViewState(
                        userLogin = response.body
                    )
                )
            }

            override fun createCall(): LiveData<GenericApiResponse<UserLogin>> {
                return RetrofitBuilder.authApiService.loginUser(userEmail, userPassword)
            }

        }.asLiveData()
    }

    fun userRegistration(
        userName: String,
        userEmail: String,
        userPassword: String
    ): LiveData<DataState<AuthViewState>> {
        return object : NetworkBoundResource<UserRegistration, AuthViewState>() {
            override fun handleApiSuccessResponse(response: ApiSuccessResponse<UserRegistration>) {
                result.value = DataState.data(
                    data = AuthViewState(
                        userRegistration = response.body
                    )
                )
            }

            override fun createCall(): LiveData<GenericApiResponse<UserRegistration>> {
                return RetrofitBuilder.authApiService.registerUser(
                    userName,
                    userEmail,
                    userPassword
                )
            }

        }.asLiveData()
    }
}
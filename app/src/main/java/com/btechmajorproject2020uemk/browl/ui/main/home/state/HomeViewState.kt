package com.btechmajorproject2020uemk.browl.ui.main.home.state

import com.btechmajorproject2020uemk.browl.models.main.DeviceCard

data class HomeViewState(

    var deviceCard: List<DeviceCard>? = null
)
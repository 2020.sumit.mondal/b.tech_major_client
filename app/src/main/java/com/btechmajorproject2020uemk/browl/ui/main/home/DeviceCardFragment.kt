package com.btechmajorproject2020uemk.browl.ui.main.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.btechmajorproject2020uemk.browl.R
import com.btechmajorproject2020uemk.browl.preference.PreferenceProvider
import com.btechmajorproject2020uemk.browl.ui.DataStateListener
import com.btechmajorproject2020uemk.browl.ui.main.MainActivity
import com.btechmajorproject2020uemk.browl.ui.main.home.state.HomeStateEvent
import com.btechmajorproject2020uemk.browl.util.Constants
import com.btechmajorproject2020uemk.browl.util.PreferenceKeys
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_device_card.*

class DeviceCardFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var dataStateListener: DataStateListener
    private lateinit var preferenceProvider: PreferenceProvider
    private var deviceId: String? = null
    private var cardName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_device_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        homeViewModel = activity?.let {
            ViewModelProvider(this).get(HomeViewModel::class.java)
        } ?: throw Exception("Invalid Activity.")

        deviceId = arguments?.getString("deviceId", null)
        cardName = arguments?.getString("cardName", null)
        setCardDetailsToUi(cardName, deviceId)

        val database = Firebase.database
        val myRef =
            database.getReferenceFromUrl("https://major-917e8.firebaseio.com/Devices/device$deviceId")

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e("FIREBASE_ERROR", p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                updateUiWithData(p0)
            }
        })

        subscribeObservers()
        preferenceProvider = PreferenceProvider(this.requireActivity().applicationContext)
        closeValveButton.setOnClickListener {
            closeTheValve(myRef)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.device_card_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.deleteDeviceCard -> deleteADeviceCard()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            dataStateListener = context as DataStateListener
        } catch (e: ClassCastException) {
            println("DEBUG: $context must implement DataStateListener.")
        }
    }

    private fun deleteADeviceCard() {
        val authToken = getAuthToken()
        triggerDeleteCardEvent(authToken, deviceId)
    }

    private fun triggerDeleteCardEvent(authToken: String, deviceId: String?) {
        deviceId?.let {
            homeViewModel.setHomeStateEvent(HomeStateEvent.DeleteCard(authToken, it))
        }
    }

    private fun getAuthToken(): String {
        var authToken = preferenceProvider.getString(PreferenceKeys.USER_AUTH_TOKEN)
        if (authToken == null) {
            authToken = ""
        }
        return authToken
    }

    private fun subscribeObservers() {
        homeViewModel.homeDataState.observe(viewLifecycleOwner, Observer { dataState ->

            println("DEBUG: HomeDataState: $dataState")

            //handling loading & message
            dataStateListener.onDataStateChange(dataState)

            //handling data
            dataState.data?.let { event ->
                event.getContentIfNotHandled()?.let { homeViewState ->
                    homeViewState.deviceCard?.let {
                        homeViewModel.setAllCards(it)
                    }
                }
            }
        })

        homeViewModel.homeViewState.observe(viewLifecycleOwner, Observer { viewState ->
            viewState.deviceCard?.let {
                navMainActivity()
            }
        })
    }

    private fun navMainActivity() {
        val intent = Intent(this.requireContext(), MainActivity::class.java)
        startActivity(intent)
        this.requireActivity().finish()
    }

    private fun closeTheValve(myRef: DatabaseReference) {
        var objectStatusData: String
        var valveStatusData: String

        myRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e("FIREBASE_ERROR", p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                objectStatusData = p0.child("objectIsPresent").value.toString()
                valveStatusData = p0.child("valveIsOpen").value.toString()

                if ((objectStatusData == "false") && (valveStatusData == "true")) {
                    myRef.child("valveIsOpen").setValue(false)
                }
            }
        })
    }

    private fun updateUiWithData(dataSnapshot: DataSnapshot) {
        configuredVolumeData.text = dataSnapshot.child("configurations/gasVolume").value.toString()
        currentVolume.text = dataSnapshot.child("gasUsageWastageVolume").value.toString()

        if (dataSnapshot.child("objectIsPresent").value.toString() == "true") {
            objectStatus.text = Constants.VALUE_YES
        } else objectStatus.text = Constants.VALUE_NO

        if (dataSnapshot.child("valveIsOpen").value.toString() == "true") {
            valveStatus.text = Constants.VALUE_YES
        } else valveStatus.text = Constants.VALUE_NO
    }

    private fun setCardDetailsToUi(cardName: String?, deviceId: String?) {
        cardNameData.text = cardName
        cardDeviceIdData.text = deviceId
    }
}

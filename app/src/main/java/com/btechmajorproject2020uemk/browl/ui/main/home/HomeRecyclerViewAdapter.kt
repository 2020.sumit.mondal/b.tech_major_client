package com.btechmajorproject2020uemk.browl.ui.main.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.btechmajorproject2020uemk.browl.R
import com.btechmajorproject2020uemk.browl.models.main.DeviceCard
import kotlinx.android.synthetic.main.list_item_card.view.*

class HomeRecyclerViewAdapter(private val interaction: Interaction? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<DeviceCard>() {

        override fun areItemsTheSame(oldItem: DeviceCard, newItem: DeviceCard): Boolean {
            return oldItem.deviceId == newItem.deviceId
        }

        override fun areContentsTheSame(oldItem: DeviceCard, newItem: DeviceCard): Boolean {
            return oldItem == newItem
        }

    }
    private val differ = AsyncListDiffer(this, diffCallback)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return DeviceCardViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_card,
                parent,
                false
            ),
            interaction
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is DeviceCardViewHolder -> {
                holder.bind(differ.currentList.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<DeviceCard>) {
        differ.submitList(list)
    }

    class DeviceCardViewHolder
    constructor(
        itemView: View,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: DeviceCard) = with(itemView) {
            itemView.setOnClickListener {
                interaction?.onItemSelected(absoluteAdapterPosition, item)
            }

            itemView.cardName.text = item.cardName
            itemView.deviceId.text = item.deviceId
        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: DeviceCard)
    }
}
package com.btechmajorproject2020uemk.browl.ui.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.btechmajorproject2020uemk.browl.R
import com.btechmajorproject2020uemk.browl.models.auth.UserRegistration
import com.btechmajorproject2020uemk.browl.preference.PreferenceProvider
import com.btechmajorproject2020uemk.browl.ui.DataStateListener
import com.btechmajorproject2020uemk.browl.ui.auth.state.AuthStateEvent
import com.btechmajorproject2020uemk.browl.ui.main.MainActivity
import com.btechmajorproject2020uemk.browl.util.PreferenceKeys
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.fragment_register.view.*

class RegisterFragment : Fragment() {

    private lateinit var viewModel: AuthViewModel
    private lateinit var dataStateListener: DataStateListener
    private lateinit var preferenceProvider: PreferenceProvider

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = activity?.run {
            ViewModelProvider(this).get(AuthViewModel::class.java)
        } ?: throw Exception("Invalid Activity.")

        subscribeObservers()
        preferenceProvider = PreferenceProvider(this.requireActivity().applicationContext)

        view.registerButton2.setOnClickListener {
            val userName = registerName.editText?.text.toString()
            val userEmail = registerEmail.editText?.text.toString()
            val userPassword = registerPassword.editText?.text.toString()
            triggerUserRegistrationEvent(userName, userEmail, userPassword)
        }
    }

    private fun subscribeObservers() {
        viewModel.authDataState.observe(viewLifecycleOwner, Observer { dataState ->

            println("DEBUG: RegisterDataState: $dataState")

            // Handling loading & message
            dataStateListener.onDataStateChange(dataState)

            // Handling data
            dataState.data?.let { event ->
                event.getContentIfNotHandled()?.let { authViewState ->
                    authViewState.userRegistration?.let {
                        // set userRegistration data
                        viewModel.setUserRegistrationData(it)
                    }
                }
            }
        })

        viewModel.authViewState.observe(viewLifecycleOwner, Observer { viewState ->

            viewState.userRegistration?.let {
                // set userRegistration data
                saveUserToSharedPreference(it)
                navMainActivity()
            }
        })
    }

    private fun saveUserToSharedPreference(user: UserRegistration) {
        user.userName?.let { userName ->
            preferenceProvider.putString(PreferenceKeys.USER_NAME, userName)
        }

        user.userEmail?.let { userEmail ->
            preferenceProvider.putString(PreferenceKeys.USER_EMAIL, userEmail)
        }

        user.authToken?.let { authToken ->
            preferenceProvider.putString(PreferenceKeys.USER_AUTH_TOKEN, authToken)
        }
    }

    private fun navMainActivity() {
        val intent = Intent(this.requireContext(), MainActivity::class.java)
        startActivity(intent)
        this.requireActivity().finish()
    }

    private fun triggerUserRegistrationEvent(
        userName: String,
        userEmail: String,
        userPassword: String
    ) {
        viewModel.setStateEvent(AuthStateEvent.RegisterUserEvent(userName, userEmail, userPassword))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            dataStateListener = context as DataStateListener
        } catch (e: ClassCastException) {
            println("DEBUG: $context must implement DataStateListener.")
        }
    }

}

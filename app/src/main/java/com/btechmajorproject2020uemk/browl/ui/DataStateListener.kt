package com.btechmajorproject2020uemk.browl.ui

import com.btechmajorproject2020uemk.browl.util.DataState

interface DataStateListener {

    fun onDataStateChange(dataState: DataState<*>?)
}
package com.btechmajorproject2020uemk.browl.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.btechmajorproject2020uemk.browl.models.auth.UserLogin
import com.btechmajorproject2020uemk.browl.models.auth.UserRegistration
import com.btechmajorproject2020uemk.browl.repository.auth.AuthRepository
import com.btechmajorproject2020uemk.browl.ui.auth.state.AuthStateEvent
import com.btechmajorproject2020uemk.browl.ui.auth.state.AuthViewState
import com.btechmajorproject2020uemk.browl.util.AbsentLiveData
import com.btechmajorproject2020uemk.browl.util.DataState

class AuthViewModel : ViewModel() {

    private val _authViewState: MutableLiveData<AuthViewState> = MutableLiveData()
    private val _authStateEvent: MutableLiveData<AuthStateEvent> = MutableLiveData()

    val authViewState: LiveData<AuthViewState> get() = _authViewState

    val authDataState: LiveData<DataState<AuthViewState>> =
        Transformations.switchMap(_authStateEvent) { stateEvent ->
            stateEvent?.let {
                handleStateEvents(it)
            }
        }

    private fun handleStateEvents(stateEvent: AuthStateEvent): LiveData<DataState<AuthViewState>> {
        return when (stateEvent) {

            is AuthStateEvent.LoginUserEvent -> {
                return AuthRepository.userLogin(stateEvent.userEmail, stateEvent.password)
            }

            is AuthStateEvent.RegisterUserEvent -> {
                return AuthRepository.userRegistration(
                    stateEvent.userName,
                    stateEvent.userEmail,
                    stateEvent.password
                )
            }

            is AuthStateEvent.None -> {
                AbsentLiveData.create()
            }
        }
    }


    // Setter methods
    private fun getCurrentViewStateOorNew(): AuthViewState {
        return authViewState.value?.let {
            it
        } ?: AuthViewState()
    }

    fun setUserLoginData(loginData: UserLogin) {
        val update = getCurrentViewStateOorNew()
        update.userLogin = loginData
        _authViewState.value = update
    }

    fun setUserRegistrationData(registrationData: UserRegistration) {
        val update = getCurrentViewStateOorNew()
        update.userRegistration = registrationData
        _authViewState.value = update
    }

    fun setStateEvent(event: AuthStateEvent) {
        _authStateEvent.value = event
    }
}
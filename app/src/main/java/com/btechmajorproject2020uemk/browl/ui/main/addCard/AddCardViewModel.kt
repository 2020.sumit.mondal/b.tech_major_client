package com.btechmajorproject2020uemk.browl.ui.main.addCard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.btechmajorproject2020uemk.browl.models.main.DeviceCard
import com.btechmajorproject2020uemk.browl.repository.main.MainRepository
import com.btechmajorproject2020uemk.browl.ui.main.addCard.state.AddCardStateEvent
import com.btechmajorproject2020uemk.browl.ui.main.addCard.state.AddCardViewState
import com.btechmajorproject2020uemk.browl.util.AbsentLiveData
import com.btechmajorproject2020uemk.browl.util.DataState

class AddCardViewModel : ViewModel() {

    private val _addCardStateEvent: MutableLiveData<AddCardStateEvent> = MutableLiveData()
    private val _addCardViewState: MutableLiveData<AddCardViewState> = MutableLiveData()

    val addCardViewState: LiveData<AddCardViewState> get() = _addCardViewState

    val addCardDataState: LiveData<DataState<AddCardViewState>> =
        Transformations.switchMap(_addCardStateEvent) { stateEvent ->
            stateEvent?.let {
                handleStateEvents(it)
            }
        }

    private fun handleStateEvents(stateEvent: AddCardStateEvent): LiveData<DataState<AddCardViewState>> {
        return when (stateEvent) {
            is AddCardStateEvent.AddNewCardEvent -> {
                return MainRepository.addCard(
                    stateEvent.authToken,
                    stateEvent.cardName,
                    stateEvent.deviceId
                )
            }
            is AddCardStateEvent.None -> {
                AbsentLiveData.create()
            }
        }
    }

    // Setter methods
    fun setAddCardStateEvent(event: AddCardStateEvent) {
        _addCardStateEvent.value = event
    }

    private fun getCurrentViewStateOrNew(): AddCardViewState {
        return addCardViewState.value?.let {
            it
        } ?: AddCardViewState()
    }

    fun setNewCardData(deviceCard: DeviceCard) {
        val update = getCurrentViewStateOrNew()
        update.deviceCard = deviceCard
        _addCardViewState.value = update
    }
}
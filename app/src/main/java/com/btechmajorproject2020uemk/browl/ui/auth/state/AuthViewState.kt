package com.btechmajorproject2020uemk.browl.ui.auth.state

import com.btechmajorproject2020uemk.browl.models.auth.UserLogin
import com.btechmajorproject2020uemk.browl.models.auth.UserRegistration

data class AuthViewState(

    var userLogin: UserLogin? = null,
    var userRegistration: UserRegistration? = null
)
package com.btechmajorproject2020uemk.browl.ui.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.btechmajorproject2020uemk.browl.R
import com.btechmajorproject2020uemk.browl.models.auth.UserLogin
import com.btechmajorproject2020uemk.browl.preference.PreferenceProvider
import com.btechmajorproject2020uemk.browl.ui.DataStateListener
import com.btechmajorproject2020uemk.browl.ui.auth.state.AuthStateEvent
import com.btechmajorproject2020uemk.browl.ui.main.MainActivity
import com.btechmajorproject2020uemk.browl.util.PreferenceKeys
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*

class LoginFragment : Fragment() {

    private lateinit var viewModel: AuthViewModel
    private lateinit var dataStateListener: DataStateListener
    private lateinit var preferenceProvider: PreferenceProvider

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = activity?.run {
            ViewModelProvider(this).get(AuthViewModel::class.java)
        } ?: throw Exception("Invalid Activity.")

        subscribeObservers()
        preferenceProvider = PreferenceProvider(this.requireActivity().applicationContext)

        view.loginButton2.setOnClickListener {
            val userEmail = loginEmail.editText?.text.toString()
            val userPassword = loginPassword.editText?.text.toString()
            triggerUserLoginEvent(userEmail, userPassword)
        }
    }

    private fun subscribeObservers() {
        viewModel.authDataState.observe(viewLifecycleOwner, Observer { dataState ->

            println("DEBUG: LoginDataState: $dataState")

            // Handling loading & message
            dataStateListener.onDataStateChange(dataState)

            // Handling data
            dataState.data?.let { event ->
                event.getContentIfNotHandled()?.let { authViewState ->
                    authViewState.userLogin?.let {
                        // set userLogin data
                        viewModel.setUserLoginData(it)
                    }
                }
            }
        })

        viewModel.authViewState.observe(viewLifecycleOwner, Observer { viewState ->

            viewState.userLogin?.let {
                // set userLogin data
                saveUserToSharedPreference(it)
                navMainActivity()
            }
        })
    }

    private fun saveUserToSharedPreference(user: UserLogin) {
        user.userName?.let { userName ->
            preferenceProvider.putString(PreferenceKeys.USER_NAME, userName)
        }

        user.userEmail?.let { userEmail ->
            preferenceProvider.putString(PreferenceKeys.USER_EMAIL, userEmail)
        }

        user.authToken?.let { authToken ->
            preferenceProvider.putString(PreferenceKeys.USER_AUTH_TOKEN, authToken)
        }
    }

    private fun navMainActivity() {
        val intent = Intent(this.requireContext(), MainActivity::class.java)
        startActivity(intent)
        this.requireActivity().finish()
    }

    private fun triggerUserLoginEvent(userEmail: String, userPassword: String) {
        viewModel.setStateEvent(AuthStateEvent.LoginUserEvent(userEmail, userPassword))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            dataStateListener = context as DataStateListener
        } catch (e: ClassCastException) {
            println("DEBUG: $context must implement DataStateListener.")
        }
    }
}

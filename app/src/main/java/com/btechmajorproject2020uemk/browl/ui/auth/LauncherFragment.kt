package com.btechmajorproject2020uemk.browl.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.btechmajorproject2020uemk.browl.R
import com.btechmajorproject2020uemk.browl.preference.PreferenceProvider
import com.btechmajorproject2020uemk.browl.ui.main.MainActivity
import com.btechmajorproject2020uemk.browl.util.PreferenceKeys
import kotlinx.android.synthetic.main.fragment_launcher.*

class LauncherFragment : Fragment() {

    private lateinit var preferenceProvider: PreferenceProvider

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_launcher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferenceProvider = PreferenceProvider(this.requireActivity().applicationContext)
        checkLastAuthenticatedUser()

        loginButton.setOnClickListener {
            navLogin()
        }

        registerButton.setOnClickListener {
            navRegister()
        }
    }

    private fun checkLastAuthenticatedUser() {
        val userName = preferenceProvider.getString(PreferenceKeys.USER_NAME)
        val userEmail = preferenceProvider.getString(PreferenceKeys.USER_EMAIL)
        val userAuthToken = preferenceProvider.getString(PreferenceKeys.USER_AUTH_TOKEN)

        if (userName != "" && userEmail != "" && userAuthToken != "") {
            navMainActivity()
        }
    }

    private fun navMainActivity() {
        val intent = Intent(this.requireContext(), MainActivity::class.java)
        startActivity(intent)
        this.requireActivity().finish()
    }

    private fun navRegister() {
        findNavController().navigate(R.id.action_launcherFragment_to_registerFragment)
    }

    private fun navLogin() {
        findNavController().navigate(R.id.action_launcherFragment_to_loginFragment)
    }

}

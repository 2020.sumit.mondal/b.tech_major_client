package com.btechmajorproject2020uemk.browl.ui.main.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.btechmajorproject2020uemk.browl.R
import com.btechmajorproject2020uemk.browl.models.main.DeviceCard
import com.btechmajorproject2020uemk.browl.preference.PreferenceProvider
import com.btechmajorproject2020uemk.browl.ui.DataStateListener
import com.btechmajorproject2020uemk.browl.ui.main.home.state.HomeStateEvent
import com.btechmajorproject2020uemk.browl.util.PreferenceKeys
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment(), HomeRecyclerViewAdapter.Interaction {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var dataStateListener: DataStateListener
    private lateinit var preferenceProvider: PreferenceProvider
    private lateinit var homeRecyclerViewAdapter: HomeRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel = activity?.let {
            ViewModelProvider(this).get(HomeViewModel::class.java)
        } ?: throw Exception("Invalid Activity.")

        subscribeObservers()
        initRecyclerView()
        preferenceProvider = PreferenceProvider(this.requireActivity().applicationContext)

        getAllCards()
    }

    private fun subscribeObservers() {
        homeViewModel.homeDataState.observe(viewLifecycleOwner, Observer { dataState ->

            println("DEBUG: HomeDataState: $dataState")

            //handling loading & message
            dataStateListener.onDataStateChange(dataState)

            //handling data
            dataState.data?.let { event ->
                event.getContentIfNotHandled()?.let { homeViewState ->
                    homeViewState.deviceCard?.let {
                        homeViewModel.setAllCards(it)
                    }
                }
            }
        })

        homeViewModel.homeViewState.observe(viewLifecycleOwner, Observer { viewState ->
            viewState.deviceCard?.let {
                homeRecyclerViewAdapter.submitList(it)
            }
        })
    }

    private fun initRecyclerView() {
        mainRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            homeRecyclerViewAdapter = HomeRecyclerViewAdapter(this@HomeFragment)
            adapter = homeRecyclerViewAdapter
        }
    }

    private fun getAllCards() {
        val authToken = getAuthToken()
        triggerGetAllCardsEvent(authToken)
    }

    private fun getAuthToken(): String {
        var authToken = preferenceProvider.getString(PreferenceKeys.USER_AUTH_TOKEN)
        if (authToken == null) {
            authToken = ""
        }
        return authToken
    }

    private fun triggerGetAllCardsEvent(authToken: String) {
        homeViewModel.setHomeStateEvent(HomeStateEvent.GetAllCardsEvent(authToken))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            dataStateListener = context as DataStateListener
        } catch (e: ClassCastException) {
            println("DEBUG: $context must implement DataStateListener.")
        }
    }

    override fun onItemSelected(position: Int, item: DeviceCard) {
        val bundle = Bundle()
        bundle.putString("deviceId", item.deviceId)
        bundle.putString("cardName", item.cardName)
        findNavController().navigate(R.id.action_navigation_home_to_deviceCardFragment, bundle)
    }
}

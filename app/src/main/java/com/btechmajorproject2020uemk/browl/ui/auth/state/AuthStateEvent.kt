package com.btechmajorproject2020uemk.browl.ui.auth.state

sealed class AuthStateEvent {

    class LoginUserEvent(
        val userEmail: String,
        val password: String
    ) : AuthStateEvent()

    class RegisterUserEvent(
        val userName: String,
        val userEmail: String,
        val password: String
    ) : AuthStateEvent()

//    class CheckPreviousAuthEvent : AuthStateEvent()

    class None : AuthStateEvent()
}
package com.btechmajorproject2020uemk.browl.ui.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.btechmajorproject2020uemk.browl.models.main.DeviceCard
import com.btechmajorproject2020uemk.browl.repository.main.MainRepository
import com.btechmajorproject2020uemk.browl.ui.main.home.state.HomeStateEvent
import com.btechmajorproject2020uemk.browl.ui.main.home.state.HomeViewState
import com.btechmajorproject2020uemk.browl.util.AbsentLiveData
import com.btechmajorproject2020uemk.browl.util.DataState

class HomeViewModel : ViewModel() {

    private val _homeStateEvent: MutableLiveData<HomeStateEvent> = MutableLiveData()
    private val _homeViewState: MutableLiveData<HomeViewState> = MutableLiveData()

    val homeViewState: LiveData<HomeViewState> get() = _homeViewState

    val homeDataState: LiveData<DataState<HomeViewState>> =
        Transformations.switchMap(_homeStateEvent) { stateEvent ->
            stateEvent?.let {
                handleStateEvents(it)
            }
    }

    private fun handleStateEvents(stateEvent: HomeStateEvent): LiveData<DataState<HomeViewState>> {
        return when (stateEvent) {
            is HomeStateEvent.GetAllCardsEvent -> {
                return MainRepository.getAllCards(stateEvent.authToken)
            }

            is HomeStateEvent.DeleteCard -> {
                return MainRepository.deleteCard(stateEvent.authToken, stateEvent.deviceId)
            }

            is HomeStateEvent.None -> {
                AbsentLiveData.create()
            }
        }
    }

    // setter methods
    fun setHomeStateEvent(event: HomeStateEvent) {
        _homeStateEvent.value = event
    }

    private fun getCurrentViewStateOrNew(): HomeViewState {
        return homeViewState.value?.let {
            it
        } ?: HomeViewState()
    }

    fun setAllCards(deviceCards: List<DeviceCard>) {
        val update = getCurrentViewStateOrNew()
        update.deviceCard = deviceCards
        _homeViewState.value = update
    }
}
package com.btechmajorproject2020uemk.browl.ui.main.addCard.state

import com.btechmajorproject2020uemk.browl.models.main.DeviceCard

data class AddCardViewState(

    var deviceCard: DeviceCard? = null
)
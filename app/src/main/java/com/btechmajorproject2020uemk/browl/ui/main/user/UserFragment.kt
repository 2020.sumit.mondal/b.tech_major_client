package com.btechmajorproject2020uemk.browl.ui.main.user

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.btechmajorproject2020uemk.browl.R
import com.btechmajorproject2020uemk.browl.preference.PreferenceProvider
import com.btechmajorproject2020uemk.browl.ui.auth.AuthActivity
import com.btechmajorproject2020uemk.browl.util.PreferenceKeys
import kotlinx.android.synthetic.main.fragment_user.*


class UserFragment : Fragment() {

    private lateinit var userViewModel: UserViewModel
    private lateinit var preferenceProvider: PreferenceProvider

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userViewModel = activity?.run {
            ViewModelProvider(this).get(UserViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        preferenceProvider = PreferenceProvider(this.requireActivity().applicationContext)

        getUserFromSharedPreference()

        logoutButton.setOnClickListener {
            logoutUser()
        }
    }

    private fun getUserFromSharedPreference() {
        val userName = preferenceProvider.getString(PreferenceKeys.USER_NAME)
        val userEmail = preferenceProvider.getString(PreferenceKeys.USER_EMAIL)
        setUserDetails(userName, userEmail)
    }

    private fun setUserDetails(userName: String?, userEmail: String?) {
        userNameField.text = userName
        userEmailField.text = userEmail
    }

    private fun logoutUser() {
        preferenceProvider.putString(PreferenceKeys.USER_NAME, "")
        preferenceProvider.putString(PreferenceKeys.USER_EMAIL, "")
        preferenceProvider.putString(PreferenceKeys.USER_AUTH_TOKEN, "")

        navAuthActivity()
    }

    private fun navAuthActivity() {
        val intent = Intent(this.requireContext(), AuthActivity::class.java)
        startActivity(intent)
        this.requireActivity().finish()
    }
}

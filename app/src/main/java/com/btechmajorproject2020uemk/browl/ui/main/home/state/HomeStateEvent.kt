package com.btechmajorproject2020uemk.browl.ui.main.home.state

sealed class HomeStateEvent {

    class GetAllCardsEvent(
        val authToken: String
    ) : HomeStateEvent()

    class DeleteCard(
        val authToken: String,
        val deviceId: String
    ) : HomeStateEvent()

    class None : HomeStateEvent()
}
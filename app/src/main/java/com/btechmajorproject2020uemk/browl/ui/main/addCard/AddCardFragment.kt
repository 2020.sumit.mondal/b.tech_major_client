package com.btechmajorproject2020uemk.browl.ui.main.addCard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.btechmajorproject2020uemk.browl.R
import com.btechmajorproject2020uemk.browl.preference.PreferenceProvider
import com.btechmajorproject2020uemk.browl.ui.DataStateListener
import com.btechmajorproject2020uemk.browl.ui.main.MainActivity
import com.btechmajorproject2020uemk.browl.ui.main.addCard.state.AddCardStateEvent
import com.btechmajorproject2020uemk.browl.util.PreferenceKeys
import kotlinx.android.synthetic.main.fragment_add_card.*
import kotlinx.android.synthetic.main.fragment_add_card.view.*

class AddCardFragment : Fragment() {

    private lateinit var addCardViewModel: AddCardViewModel
    private lateinit var dataStateListener: DataStateListener
    private lateinit var preferenceProvider: PreferenceProvider

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addCardViewModel = activity?.run {
            ViewModelProvider(this).get(AddCardViewModel::class.java)
        } ?: throw Exception("Invalid Activity.")

        subscribeObservers()
        preferenceProvider = PreferenceProvider(this.requireActivity().applicationContext)

        view.addCardButton.setOnClickListener {
            val authToken = getAuthToken()
            val cardName = inputCardName.editText?.text.toString()
            val deviceId = inputDeviceId.editText?.text.toString()
            triggerAddNewCardEvent(authToken, cardName, deviceId)
        }
    }

    private fun subscribeObservers() {
        addCardViewModel.addCardDataState.observe(viewLifecycleOwner, Observer { dataState ->

            println("DEBUG: AddCardDataState: $dataState")

            //Handling loading and message
            dataStateListener.onDataStateChange(dataState)

            //Handling data
            dataState.data?.let { event ->
                event.getContentIfNotHandled()?.let { addCardViewState ->
                    addCardViewState.deviceCard?.let {
                        addCardViewModel.setNewCardData(it)
                    }
                }
            }
        })

        addCardViewModel.addCardViewState.observe(viewLifecycleOwner, Observer { viewState ->

            viewState.deviceCard?.let {
                navMainActivity()
            }
        })
    }

    private fun navMainActivity() {
        val intent = Intent(this.requireContext(), MainActivity::class.java)
        startActivity(intent)
        this.requireActivity().finish()
    }

    private fun getAuthToken(): String {
        var authToken = preferenceProvider.getString(PreferenceKeys.USER_AUTH_TOKEN)
        if (authToken == null) {
            authToken = ""
        }
        return authToken
    }

    private fun triggerAddNewCardEvent(authToken: String, cardName: String, deviceId: String) {
        addCardViewModel.setAddCardStateEvent(
            AddCardStateEvent.AddNewCardEvent(
                authToken,
                cardName,
                deviceId
            )
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            dataStateListener = context as DataStateListener
        } catch (e: ClassCastException) {
            println("DEBUG: $context must implement DataStateListener.")
        }
    }
}

package com.btechmajorproject2020uemk.browl.ui.main.addCard.state

sealed class AddCardStateEvent {

    class AddNewCardEvent(
        val authToken: String,
        val cardName: String,
        val deviceId: String
    ) : AddCardStateEvent()

    class None : AddCardStateEvent()
}
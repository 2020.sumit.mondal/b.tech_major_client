package com.btechmajorproject2020uemk.browl.preference

import android.content.Context
import com.btechmajorproject2020uemk.browl.util.PreferenceKeys

class PreferenceProvider(context: Context) {

    private val sharedPreferences =
        context.getSharedPreferences(PreferenceKeys.APP_PREFERENCES, Context.MODE_PRIVATE)

    // Save strings to the shared preferences
    fun putString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    // Get string from the shared preferences
    fun getString(key: String): String? {
        return sharedPreferences.getString(key, null)
    }
}
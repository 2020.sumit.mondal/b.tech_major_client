package com.btechmajorproject2020uemk.browl.api.auth

import androidx.lifecycle.LiveData
import com.btechmajorproject2020uemk.browl.models.auth.UserLogin
import com.btechmajorproject2020uemk.browl.models.auth.UserRegistration
import com.btechmajorproject2020uemk.browl.util.GenericApiResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthApiService {

    @POST("auth")
    @FormUrlEncoded
    fun loginUser(
        @Field("email") userEmail: String,
        @Field("password") password: String
    ): LiveData<GenericApiResponse<UserLogin>>

    @POST("users/user/signup")
    @FormUrlEncoded
    fun registerUser(
        @Field("name") userName: String,
        @Field("email") userEmail: String,
        @Field("password") password: String
    ): LiveData<GenericApiResponse<UserRegistration>>
}
package com.btechmajorproject2020uemk.browl.api.main

import androidx.lifecycle.LiveData
import com.btechmajorproject2020uemk.browl.models.main.DeviceCard
import com.btechmajorproject2020uemk.browl.util.GenericApiResponse
import retrofit2.http.*

interface UserApiService {

    @POST("cards/card/create")
    @FormUrlEncoded
    fun addNewCard(
        @Header("x-auth-token") authToken: String,
        @Field("name") cardName: String,
        @Field("deviceId") deviceId: String
    ): LiveData<GenericApiResponse<DeviceCard>>

    @GET("cards/card/user")
    fun getAllCards(
        @Header("x-auth-token") authToken: String
    ): LiveData<GenericApiResponse<List<DeviceCard>>>

    @DELETE("cards/card/{deviceId}/delete")
    fun deleteCard(
        @Header("x-auth-token") authToken: String,
        @Path("deviceId") deviceId: String
    ): LiveData<GenericApiResponse<List<DeviceCard>>>
}
package com.btechmajorproject2020uemk.browl.api

import com.btechmajorproject2020uemk.browl.api.auth.AuthApiService
import com.btechmajorproject2020uemk.browl.api.main.UserApiService
import com.btechmajorproject2020uemk.browl.util.LiveDataCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    private const val BASE_URL = "https://btech-projectserver-browl-2020.herokuapp.com/api/"
    private val retrofitBuilder: Retrofit.Builder by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
    }

    val authApiService: AuthApiService by lazy {
        retrofitBuilder
            .build()
            .create(AuthApiService::class.java)
    }

    val userApiService: UserApiService by lazy {
        retrofitBuilder
            .build()
            .create(UserApiService::class.java)
    }
}
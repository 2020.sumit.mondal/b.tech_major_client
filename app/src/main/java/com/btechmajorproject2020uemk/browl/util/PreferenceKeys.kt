package com.btechmajorproject2020uemk.browl.util

class PreferenceKeys {

    companion object {

        // Shared preference files:
        const val APP_PREFERENCES: String = "com.btechmajorproject2020uemk.browl.APP_PREFERENCES"

        // Shared preference keys:
        const val USER_NAME: String = "com.btechmajorproject2020uemk.browl.USER_NAME"
        const val USER_EMAIL: String = "com.btechmajorproject2020uemk.browl.USER_EMAIL"
        const val USER_AUTH_TOKEN: String = "com.btechmajorproject2020uemk.browl.USER_AUTH_TOKEN"
    }
}
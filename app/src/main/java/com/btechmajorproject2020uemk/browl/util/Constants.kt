package com.btechmajorproject2020uemk.browl.util

class Constants {

    companion object {
        const val BASE_URL = "https://btech-projectserver-browl-2020.herokuapp.com/api/"
        const val ACCOUNT_PROPERTIES_PK = 1
        const val AUTH_TOKEN_PK = 2

        const val VALUE_YES = "YES"
        const val VALUE_NO = "NO"
    }
}